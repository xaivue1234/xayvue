const int ledPin1 = 8;     
const int ledPin2 = 7;     
const int switchPin = 2;   

void setup() {
  pinMode(ledPin1, OUTPUT);
  pinMode(ledPin2, OUTPUT);
  pinMode(switchPin, INPUT_PULLUP); 
}

void loop() {
  int switchState = digitalRead(switchPin); 
  
  if (switchState == LOW) {
    digitalWrite(ledPin1, HIGH);  
    delay(1000);                  
    digitalWrite(ledPin1, LOW);   
    digitalWrite(ledPin2, HIGH);  
    delay(1000);                  
    digitalWrite(ledPin2, LOW);   
  } else {
    digitalWrite(ledPin1, LOW);   
    digitalWrite(ledPin2, LOW);   
  }
}
